# Usa a imagem oficial do Python como base
FROM python:3.8
# Define o diretório de trabalho dentro do contêiner
WORKDIR /app
# Copia os requisitos do projeto e instala as dependências
COPY ./test_pipeline_ci_cd/requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

# Copia todo o projeto para o diretório de trabalho do contêiner
COPY . /app/

# Expõe a porta em que o servidor Django estará executando
EXPOSE 8000

# Comando para executar o servidor Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]